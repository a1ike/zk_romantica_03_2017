$(document).ready(function() {

  $('#owl-full').owlCarousel({

    navigation: true,
    slideSpeed: 300,
    paginationSpeed: 400,
    items: 1,
    itemsDesktop: false,
    itemsDesktopSmall: false,
    itemsDesktopSmall: [979, 1],
    itemsDesktopSmall: [768, 1],
    itemsTablet: false,
    itemsMobile: false,
    navigation: true,
    navigationText: [
      '<img src=\'images/icon_owl_left.png\'>',
      '<img src=\'images/icon_owl_right.png\'>'
    ]

  });


  $('.owl-carousel').owlCarousel({
    items: 4,
    navigation: true,
    itemsDesktopSmall: true,
    itemsDesktopSmall: [979, 3],
    itemsDesktopSmall: [768, 1],
    navigationText: [
      '<img src=\'images/icon_owl_left.png\'>',
      '<img src=\'images/icon_owl_right.png\'>'
    ]
  });

  $('#menuToggle').on('click', function() {
    $('.slide-nav').toggleClass('slide-nav_active');
  });

  $('#popupCallToggle').on('click', function() {
    $('#popupCall').toggleClass('popup-win_active');
  });

  $('#popupCallClose').on('click', function() {
    $('#popupCall').removeClass('popup-win_active');
  });

  $('.popup-flat-win').on('click', function() {
    $('#popupFlat').toggleClass('popup-win_active');
  });

  $('#popupFlatClose').on('click', function() {
    $('#popupFlat').removeClass('popup-win_active');
  });

  $('#popupMapToggle').on('click', function() {
    $('#popupMap').toggleClass('popup-win_active');
  });

  $('#popupMapClose').on('click', function() {
    $('#popupMap').removeClass('popup-win_active');
  });

});